provider "aws" {
  profile    = "default"
  region     = "us-east-1"
}


resource "aws_instance" "example" {
  ami           = "ami-b374d5a5"
  instance_type = "t2.micro"
  key_name = aws_key_pair.deployer.key_name
  associate_public_ip_address = true
  security_groups = [ aws_security_group.http_and_ssh.id ]
}

resource "aws_security_group" "http_and_ssh" {
  name        = "allow_http_and_ssh"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}


resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDT/hjhJs7TCPR/27MeWflNU8eXQeXkInfaKfELljYsrjTzZXmhFq0BIUAzty9nsiafZCntNNDtw9bdiUbVYYdb9yba2gHSDG3nMrWcuKQWC4JKA6EHGobqA2Z8cpiHAyubo3nJjgx7m+JSoeHLX+nAMFxbmiGDPQL3CZCAsRl0YI3lf7eJf3GQSCXFlqJSmgvp3UwQi1xkO6KSdLCRlFRL2oZgRgqZNu/yIjIE3+TkrxGt1oJVcnJxCyfChmdVkl1baxvzl+CxXYacgljK+2DH0NcgQ1Frbgtg38R6oQBxQsAXK+8q9sebNtZWj0KKJ77SKUGt3xcw+o+YdbG2SkLR ljtirazona@ThinkPad-E470"
}
